"use strict";
$(document).ready(function(){
  /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    /** Các biến toàn cục hằng số Form mode: 4 trạng thái của form. Mặc định sẽ là Normal
     *
     ** Khi ấn vào nút Thêm, cần đổi biến trạng thái về trạng thái Insert
    ** Khi ấn vào nút Sửa, cần đổi biến trạng thái về trạng thái Update
    ** Khi ấn vào nút Xóa, cần đổi biến trạng thái về trạng thái Delete
    *
    * Tại một thời điểm, trạng thái của form luôn là 1 trong 4 trạng thái
    */ 
    var gFORM_MODE_NORMAL = "Normal";
    var gFORM_MODE_INSERT = "Insert";
    var gFORM_MODE_UPDATE = "Update";
    var gFORM_MODE_DELETE = "Delete";

    //Biến toàn cục cho trạng thái của form: mặc định ban đầu là trạng thái Normal
    var gFormMode = gFORM_MODE_NORMAL;

    //Biến toàn cục chứa dữ liệu nhập mới
    var gClassObject = {
        classCode: "",
        className: "",
        teacher: "",
        phoneNumber: ""
    }
    //Biến toàn cục chứa studentId
    var gClassId = 0;
    //Biến toàn cục Api
    const gBASE_URL = "http://localhost:8080/class/";

    //Biến toàn cục chứa dữ liệu lớp học
    var gClass_db = {
        classRoom:[],
        // Thực hiện filter roles
        filterClass: function(paramClassFilter){
            var vClass = [];
            // cần thực hiện trả lại 01 array để display trong bảng
            vClass = this.classRoom.filter(function(paramClass){
            return(
                    (paramClass.id == paramClassFilter.classId || paramClassFilter.classId == 0) &&
                    (paramClass.id == paramClassFilter.teacher || paramClassFilter.teacher == 0)
                );
            });
            return vClass;
        }
    };
    // Biến mảng hằng số chứa danh sách tên các thuộc tính
    const gCLASS_COLS = ["stt", "classCode", "className", "teacher","phoneNumber", "detail"];
    
    // Biến mảng toàn cục định nghĩa chỉ số các cột tương ứng
    const gSTT = 0;
    const gCLASS_CODE = 1;
    const gCLASS_NAME = 2;
    const gTEACHER = 3;
    const gPHONE_NUMBER = 4;
    const gDETAIL = 5;

    //Biến toàn cục stt
    var gStt = 1;

    //Khai báo data table
    var gClassTable = $("#class-table").DataTable({
        columns:[
            {"data": gCLASS_COLS[gSTT]},
            {"data": gCLASS_COLS[gCLASS_CODE]},
            {"data": gCLASS_COLS[gCLASS_NAME]},
            {"data": gCLASS_COLS[gTEACHER]},
            {"data": gCLASS_COLS[gPHONE_NUMBER]},
            {"data": gCLASS_COLS[gDETAIL]}
        ],
        columnDefs:[
            { // định nghĩa lại cột STT
            targets: gSTT,
            render: function() {
                return gStt ++;
            }
            },
            {//định nghĩa cột detail
            targets: gDETAIL,
            defaultContent: `
                <button class="btn btn-warning detail">
                <i class="fas fa-info-circle fa-lg"></i>
                </button>
            `
            }
        ]
    });

    /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
    onPageLoading();
    //Gán sự kiện change list student khi chọn lớp học
    $("#class-select").on("change", onBtnChangeClassList);
    //Gán sự kiện cho nút detail
    $("#class-table tbody").on("click", ".detail", function(){
        onBtnClickDetail(this);
    });
    //Gán sự kiện cho nút tạo mới học sinh
    $("#save-class").on("click", onBtnCreateClassClick);
    //Gán sự kiện cho nút cập nhật học sinh
    $(document).on("click", ".update", onBtnUpdateClassClick);
    //Gán sự kiện cho nút xóa học sinh
    $("#delete-class").on("click", onBtnDeleteClassById);
    //Gán sự kiện cho nút xóa tất cả học sinh
    $("#delete-all-class").on("click", onBtnDeleteAllClass);
    //Gán sự kiện cho nút xác nhận xóa học sinh
    $("#confirm-delete-class").on("click", onBtnConfirmDelete);
    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    //Hàm load trang
    function onPageLoading(){
        callApiToGetAllClass();
    };
    //Hàm xử lý sự kiện thay đổi lớp học
    function onBtnChangeClassList(){
        //Khai báo đối tượng chứa dữ liệu
        var vClassObj = {
            classId: "",
            teacher: ""
        }
        //B1: Get data
        getClassData(vClassObj);
        //B2: Thực nghiệp vụ lọc dữ liệu
        var vClassFilter = gClass_db.filterClass(vClassObj)
        //B3: Gọi api lấy dữ liệu student
        loadDataToTable(vClassFilter);
    };
    //Hàm xử lý sự kiện click nút detail
    function onBtnClickDetail(paramDetail){
        $("#save-class")
            .addClass("btn-warning update")
            .html("Cập nhật thông tin");
        $("#delete-class").prop("style", "block");
        //B1: get data
        var vClassId = getDataFromRow(paramDetail);
        //B2: validate data
        if(vClassId !== null){
            //B3: Call Api
            callApiToGetClassById(vClassId);
        }

    };
    //Hàm xử lý sự kiện tạo mới học sinh
    function onBtnCreateClassClick(){
        gFormMode = gFORM_MODE_INSERT;
        //B1: Get data
        getDataForm(gClassObject);
        //B2: Vaidation data
        var vIsValid = vaidateData(gClassObject);
        if (vIsValid) {
            //B3: Call api update data
            callApiToCreateClass(gClassObject);
        }
    }
    //Hàm xử lý sự kiện cập nhật học sinh
    function onBtnUpdateClassClick(){
        gFormMode = gFORM_MODE_UPDATE;
        //B1: Get data
        getDataForm(gClassObject);
        //B2: Vaidation data
        var vIsValid = vaidateData(gClassObject);
        if (vIsValid) {
            //B3: Call api update data
            callApiToUpdateClass(gClassObject);
        }
    }
    //Hàm xử lý sự kiện xóa học sinh theo id
    function onBtnDeleteClassById(){
        gFormMode = gFORM_MODE_DELETE;
        $("#modal-delete-class").modal("show");
        
    };
    //Hàm xử lý sự kiện confirm xóa học sinh
    function onBtnConfirmDelete(){
        gClassId = gClass_db.classRoom.id;
        $.ajax({
        url: gBASE_URL + "delete/" + gClassId,
        type: "DELETE",
        data: "json",
        success: function(responseObject){
            alert("Delete classId: " +  gClassId +" success!");
            location.reload();
        },
            error: function(error){
            console.log(error);
        }
        });
    };
    //Hàm xử lý sự kiện xóa tất cả học sinh
    function onBtnDeleteAllClass(){
        gFormMode = gFORM_MODE_DELETE;
        $.ajax({
        url: gBASE_URL + "delete/all",
        type: "DELETE",
        data: "json",
        success: function(responseObject){
            alert("Delete all student success!");
            location.reload();
        },
            error: function(error){
            console.log(error);
        }
        });
    };
    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình */
    //Hàm gọi Api lấy all student
    function callApiToGetAllClass(){
        $.ajax({
        url: gBASE_URL + "all",
        type: "GET",
        dataType: "json",
        success: function(res){
            gClass_db.classRoom = res;
            loadDataToTable(gClass_db.classRoom);
            loadClassToSelect(gClass_db.classRoom);
            //console.log(gStudent_db.students);
            
        },
        error: function(err){
            console.log(err.response);
        }
        });
    };
    //Hàm gọi Api lấy voucher theo Id
    function callApiToGetClassById(paramClassId){
        $.ajax({
        url: gBASE_URL + "detail/" + paramClassId,
        type: "GET",
        dataType: "json",
        success: function(res){
            gClass_db.classRoom = res;
            showClassToForm(gClass_db.classRoom);
        },
        error: function(err){
            console.log(err.response);
        }
        });
    };
    //Hàm gọi Api tạo học sinh mới
    function callApiToCreateClass(paramClassObject){
        $.ajax({
            url: gBASE_URL + "create",
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify(paramClassObject),
            success: function(res){
            alert("Thêm lớp học thành công");
            location.reload();
            
            },
            error: function(err){
                console.error("400: Mã số lớp học này đã tồn tại");
                alert("Mã số lớp học này đã tồn tại");
            }
        });
    };
    //Hàm gọi Api cập nhật học sinh
    function callApiToUpdateClass(paramClassObj){
        gClassId = gClass_db.classRoom.id;
        $.ajax({
            url: gBASE_URL + "update/" + gClassId,
            type: "PUT",
            contentType: "application/json",
            data: JSON.stringify(paramClassObj),
            success: function(res){
            alert("Cập nhật lớp thành công");
            location.reload();
            
            },
            error: function(err){
            console.log(err.response);
            }
        });
    };
    //Load lớp học lên ô select
    function loadClassToSelect(paramClass){
        for (let index = 0; index < paramClass.length; index++) {
            $("#class-select").append($("<option>", {
                value: paramClass[index].id,
                text: paramClass[index].classCode
            }));
            $("#teacher-select").append($("<option>", {
                value: paramClass[index].id,
                text: paramClass[index].teacher
            }));
        }
    };
    //Hàm lấy ra data theo row
    function getDataFromRow(paramDetail){
        var vSelectRow = $(paramDetail).parents("tr")
        var vRowData = gClassTable.row(vSelectRow).data();
        //console.log(vRowData);
        return vRowData.id;
    };
    //Hàm thu thập dữ liệu
    function getDataForm(paramClassObject){
        paramClassObject.classCode = $("#input-class-code").val().trim();
        paramClassObject.className = $("#input-class-name").val().trim();
        paramClassObject.teacher = $("#input-teacher-name").val().trim();
        paramClassObject.phoneNumber = $("#input-phone-number").val().trim();
    }
    //Hàm kiểm tra dữ liệu
    function vaidateData(paramClassObject){
        var vResult = true;
        if (paramClassObject.classCode == "") {
            alert("Vui lòng nhập mã số lớp học.");
            vResult = false;
        }
        if (paramClassObject.className == "") {
            alert("Vui lòng nhập tên lớp học.");
            vResult = false;
        }
        if (paramClassObject.teacher == "") {
            alert("Vui lòng nhập tên giáo viên.");
            vResult = false;
        }
        if (paramClassObject.phoneNumber == "" || 
            paramClassObject.phoneNumber.length < 9 || 
            isNaN(paramClassObject.phoneNumber)) {
            alert("Vui lòng nhập số điện thoại.");
            vResult = false;
        }
        return vResult;
    };
    
    //Hàm load voucher lên form
    function showClassToForm(paramClass){
    console.log(paramClass);
    
    $("#input-class-code").val(paramClass.classCode);
    $("#input-class-name").val(paramClass.className);
    $("#input-teacher-name").val(paramClass.teacher);
    $("#input-phone-number").val(paramClass.phoneNumber);
    
    };
    //Hàm lấy classId
    function getClassData(paramClassObj){
        paramClassObj.classId = $("#class-select option:selected").val();
        paramClassObj.teacher = $("#teacher-select option:selected").val();
    }
    //Hàm load dữ liệu lên table
    function loadDataToTable(paramClass){
        gStt = 1;
        gClassTable.clear();
        gClassTable.rows.add(paramClass);
        gClassTable.draw();
    };

});