"use strict";
$(document).ready(function(){
  /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
  /** Các biến toàn cục hằng số Form mode: 4 trạng thái của form. Mặc định sẽ là Normal
 *
 ** Khi ấn vào nút Thêm, cần đổi biến trạng thái về trạng thái Insert
 ** Khi ấn vào nút Sửa, cần đổi biến trạng thái về trạng thái Update
 ** Khi ấn vào nút Xóa, cần đổi biến trạng thái về trạng thái Delete
 *
 * Tại một thời điểm, trạng thái của form luôn là 1 trong 4 trạng thái
 */ 
  var gFORM_MODE_NORMAL = "Normal";
  var gFORM_MODE_INSERT = "Insert";
  var gFORM_MODE_UPDATE = "Update";
  var gFORM_MODE_DELETE = "Delete";

  //Biến toàn cục cho trạng thái của form: mặc định ban đầu là trạng thái Normal
  var gFormMode = gFORM_MODE_NORMAL;

  //Biến toàn cục chứa dữ liệu nhập mới
  var gStudentObject = {
    studentCode: "",
    studentName: "",
    gender: "",
    birthday: "",
    address: "",
    phone: "",
  }
  //Biến toàn cục chứa studentId
  var gStudentId = 0;
  //Biến toàn cục Api
  const gBASE_URL = "http://localhost:8080/student/";

  //Biến toàn cục chứa dữ liệu học sinh
  var gStudent_db = {
    students:[]
  };
  // Biến mảng hằng số chứa danh sách tên các thuộc tính
  const gSTUDENT_COLS = ["stt", "studentCode", "studentName", "gender","birthday","address", "phone", "detail"];
  
  // Biến mảng toàn cục định nghĩa chỉ số các cột tương ứng
  const gSTT = 0;
  const gSTUDENT_CODE = 1;
  const gSTUDENT_NAME = 2;
  const gGENDER = 3;
  const gBIRTHDAY = 4;
  const gADDRESS = 5;
  const gPHONE = 6;
  const gDETAIL = 7;
  //Biến toàn cục chứa dữ liệu lớp học
  var gClass_db = {
    classRoom:[]
  };
  //Biến toàn cục stt
  var gStt = 1;

  //Khai báo data table
  var gStudentTable = $("#student-table").DataTable({
      columns:[
        {"data": gSTUDENT_COLS[gSTT]},
        {"data": gSTUDENT_COLS[gSTUDENT_CODE]},
        {"data": gSTUDENT_COLS[gSTUDENT_NAME]},
        {"data": gSTUDENT_COLS[gGENDER]},
        {"data": gSTUDENT_COLS[gBIRTHDAY]},
        {"data": gSTUDENT_COLS[gADDRESS]},
        {"data": gSTUDENT_COLS[gPHONE]},
        {"data": gSTUDENT_COLS[gDETAIL]}
      ],
      columnDefs:[
        { // định nghĩa lại cột STT
          targets: gSTT,
          render: function() {
            return gStt ++;
          }
        },
        {//định nghĩa cột detail
          targets: gDETAIL,
          defaultContent: `
            <button class="btn btn-warning detail">
            <i class="fas fa-info-circle fa-lg"></i>
            </button>
          `
        }
      ]
  });

  /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
  onPageLoading();
  //Gán sự kiện change list student khi chọn lớp học
  $("#class-select").on("change", onBtnChangeStudentList);
  //Gán sự kiện cho nút detail
  $("#student-table tbody").on("click", ".detail", function(){
    onBtnClickDetail(this);
  });
  //Gán sự kiện cho nút tạo mới học sinh
  $("#save-student").on("click", onBtnCreateStudentClick);
  //Gán sự kiện cho nút cập nhật học sinh
  $(document).on("click", ".update", onBtnUpdateStudentClick);
  //Gán sự kiện cho nút xóa học sinh
  $("#delete-student").on("click", onBtnDeleteStudentById);
  //Gán sự kiện cho nút xóa tất cả học sinh
  $("#delete-all-student").on("click", onBtnDeleteAllStudent);
  //Gán sự kiện cho nút xác nhận xóa học sinh
  $("#confirm-delete-student").on("click", onBtnConfirmDelete);
  /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
  //Hàm load trang
  function onPageLoading(){
    callApiToGetAllStudent();
    callApiToGetAllClass();
  };
  //Hàm xử lý sự kiện thay đổi lớp học
  function onBtnChangeStudentList(){
    //Khai báo đối tượng chứa dữ liệu
    var vClassObj = {
      classId: ""
    }
    //B1: Get data
    getClassData(vClassObj);
    //B2: Kiểm tra dữ liệu(ko cần)
    //B3: Gọi api lấy dữ liệu student
    callApiToGetStudentByClassId(vClassObj);
  };
  //Hàm xử lý sự kiện click nút detail
  function onBtnClickDetail(paramDetail){
    $("#save-student")
				.addClass("btn-warning update")
				.html("Cập nhật thông tin");
			$("#delete-student").prop("style", "block");
			//B1: get data
			var vStudentId = getDataFromRow(paramDetail);
			//B2: validate data
			if(vStudentId !== null){
				//B3: Call Api
				callApiToGetStudentById(vStudentId);
			}

  };
  //Hàm xử lý sự kiện tạo mới học sinh
  function onBtnCreateStudentClick(){
    gFormMode = gFORM_MODE_INSERT;
    //B1: Get data
    getDataForm(gStudentObject);
    //B2: Vaidation data
    var vIsValid = vaidateDataStudent(gStudentObject);
    if (vIsValid) {
      //B3: Call api update data
      callApiToCreateStudent(gStudentObject);
    }
  }
  //Hàm xử lý sự kiện cập nhật học sinh
  function onBtnUpdateStudentClick(){
    gFormMode = gFORM_MODE_UPDATE;
    //B1: Get data
    getDataForm(gStudentObject);
    //B2: Vaidation data
    var vIsValid = vaidateData(gStudentObject);
    if (vIsValid) {
      //B3: Call api update data
      callApiToUpdateStudent(gStudentObject);
    }
  }
  //Hàm xử lý sự kiện xóa học sinh theo id
  function onBtnDeleteStudentById(){
    gFormMode = gFORM_MODE_DELETE;
    $("#modal-delete-student").modal("show");
    
  };
  //Hàm xử lý sự kiện confirm xóa học sinh
  function onBtnConfirmDelete(){
    gStudentId = gStudent_db.students.id;
    $.ajax({
      url: gBASE_URL + "delete/" + gStudentId,
      type: "DELETE",
      data: "json",
      success: function(responseObject){
        alert("Delete studentId: " +  gStudentId +" success!");
        location.reload();
      },
        error: function(error){
        console.log(error);
      }
    });
  };
  //Hàm xử lý sự kiện xóa tất cả học sinh
  function onBtnDeleteAllStudent(){
    gFormMode = gFORM_MODE_DELETE;
    $.ajax({
      url: gBASE_URL + "delete/all",
      type: "DELETE",
      data: "json",
      success: function(responseObject){
        alert("Delete all student success!");
        location.reload();
      },
        error: function(error){
        console.log(error);
      }
    });
  };
  /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình */
  //Hàm gọi Api lấy all student
  function callApiToGetAllStudent(){
    $.ajax({
      url: gBASE_URL + "all",
      type: "GET",
      dataType: "json",
      success: function(res){
        gStudent_db.students = res;
        loadDataToTable(gStudent_db.students);
        //console.log(gStudent_db.students);
        
      },
      error: function(err){
        console.log(err.response);
      }
    });
  };
  //Hàm gọi Api lấy all class
  function callApiToGetAllClass(){
    const vBASE_URL = "http://localhost:8080/class/"
    $.ajax({
      url: vBASE_URL + "all",
      type: "GET",
      dataType: "json",
      success: function(res){
        gClass_db.classRoom = res;
        loadClassToSelect(gClass_db.classRoom);
        //console.log(gClass_db.classRoom);
        
      },
      error: function(err){
        console.log(err.response);
      }
    });
  };
  //Hàm gọi Api lấy student by class id
  function callApiToGetStudentByClassId(paramClassObj){
    if (paramClassObj.classId == 0) {
      callApiToGetAllStudent();
    }else{
      $.ajax({
        url: gBASE_URL + paramClassObj.classId + "/class",
        type: "GET",
        dataType: "json",
        success: function(res){
          gStudent_db.students = res;
          loadDataToTable(gStudent_db.students);
          //console.log(gStudent_db.students);
          
        },
        error: function(err){
          console.log(err.response);
        }
      });
    }
  };
  //Hàm gọi Api lấy voucher theo Id
  function callApiToGetStudentById(paramStudentId){
    $.ajax({
      url: gBASE_URL + "detail/" + paramStudentId,
      type: "GET",
      dataType: "json",
      success: function(res){
        gStudent_db.students = res;
        //console.log(gStudent_db.students);
        showStudentToForm(gStudent_db.students);
      },
      error: function(err){
        console.log(err.response);
      }
    });
  };
  //Hàm gọi Api tạo học sinh mới
  function callApiToCreateStudent(paramStudentObject){
    let vClassId = $("#class-select option:selected").val();
    if (vClassId == "0") {
      alert("Vui lòng chọn lớp học");
      return false
    }else{
      $.ajax({
        url: gBASE_URL + "create/" + vClassId,
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify(paramStudentObject),
        success: function(res){
          alert("Thêm học sinh thành công");
          location.reload();
          
        },
        error: function(err){
          console.error("400: Mã số học sinh này đã tồn tại");
          alert("Mã số học sinh này đã tồn tại");
        }
      });
    }
  };
  //Hàm gọi Api cập nhật học sinh
  function callApiToUpdateStudent(paramStudentObject){
    gStudentId = gStudent_db.students.id;
      $.ajax({
        url: gBASE_URL + "update/" + gStudentId,
        type: "PUT",
        contentType: "application/json",
        data: JSON.stringify(paramStudentObject),
        success: function(res){
          alert("Cập nhật học sinh thành công");
          location.reload();
          
        },
        error: function(err){
          console.log(err.response);
        }
      });
  };
  //Load lớp học lên ô select
  function loadClassToSelect(paramClass){
    for (let index = 0; index < paramClass.length; index++) {
      $("#class-select").append($("<option>", {
        value: paramClass[index].id,
        text: paramClass[index].classCode
      }));
    }
  };
  //Hàm lấy ra data theo row
  function getDataFromRow(paramDetail){
    var vSelectRow = $(paramDetail).parents("tr")
    var vRowData = gStudentTable.row(vSelectRow).data();
    //console.log(vRowData);
    return vRowData.id;
  };
  //Hàm thu thập dữ liệu
  function getDataForm(paramStudentObject){
    paramStudentObject.studentCode = $("#inp-student-code").val().trim();
    paramStudentObject.studentName = $("#inp-student-name").val().trim();
    paramStudentObject.address = $("#inp-addres").val().trim();
    paramStudentObject.phone = $("#inp-phone").val().trim();
    paramStudentObject.birthday = $("#inp-birthday").val().trim();
    paramStudentObject.gender = $('input[name="gender"]:checked').val();
  }
  //Hàm kiểm tra dữ liệu
  function vaidateDataStudent(paramStudentObject){
    var vResult = true;
    try {
      if (paramStudentObject.studentCode == "") {
        alert("Vui lòng nhập mã số học sinh.");
        vResult = false;
      }
      if (paramStudentObject.studentName == "") {
        alert("Vui lòng nhập tên học sinh.");
        vResult = false;
      }
      if (paramStudentObject.address == "") {
        alert("Vui lòng nhập địa chỉ.");
        vResult = false;
      }
      if (paramStudentObject.phone == "" || 
          paramStudentObject.phone.length < 9 || 
          isNaN(paramStudentObject.phone)) {
        alert("Vui lòng nhập số điện thoại.");
        vResult = false;
      }
      if (paramStudentObject.birthday == "") {
        alert("Vui lòng nhập ngày sinh dd/mm/yyyy.");
        vResult = false;
      }
    } catch (error) {
      alert(e);
    }
    return vResult;
  };
  //Hàm kiểm tra dữ liệu
  function vaidateData(paramStudentObject){
    var vResult = true;
      if (paramStudentObject.studentCode == "") {
        alert("Vui lòng nhập mã số học sinh.");
        vResult = false;
      }
      if (paramStudentObject.studentName == "") {
        alert("Vui lòng nhập tên học sinh.");
        vResult = false;
      }
      if (paramStudentObject.address == "") {
        alert("Vui lòng nhập địa chỉ.");
        vResult = false;
      }
      if (paramStudentObject.phone == "" || 
          paramStudentObject.phone.length < 9 || 
          isNaN(paramStudentObject.phone)) {
        alert("Vui lòng nhập số điện thoại.");
        vResult = false;
      }
      if (paramStudentObject.birthday == "") {
        alert("Vui lòng nhập ngày sinh dd/mm/yyyy.");
        vResult = false;
      }
      return vResult;
  };
  //Hàm định dạng ngày/tháng
  function formatDate(paramDate) {
    "use strict";
    var d = new Date(paramDate);
    var month = '' + (d.getMonth()+1);
    var day = '' + d.getDate();
    var year = d.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;

    return [year, month, day].join('-');
  };
  //Hàm load voucher lên form
  function showStudentToForm(paramStudents){
  //console.log(paramStudents);
  var vBirthdayDate = new Date(paramStudents.birthday);
  var vBirthDay = formatDate(vBirthdayDate.toDateString());
  
  $("#inp-student-code").val(paramStudents.studentCode);
  $("#inp-student-name").val(paramStudents.studentName);
  $("#inp-addres").val(paramStudents.address);
  $("#inp-phone").val(paramStudents.phone);
  $("#inp-birthday").val(vBirthDay);
    if (paramStudents.gender == "Nữ") {
      $("#female").prop("checked",true);
    }else{
      $("#male").prop("checked",true);
    }
  };
  //Hàm lấy classId
  function getClassData(paramClassObj){
    paramClassObj.classId = $("#class-select option:selected").val();
  }
  //Hàm load dữ liệu lên table
  function loadDataToTable(paramStudents){
    gStt = 1;
    gStudentTable.clear();
    gStudentTable.rows.add(paramStudents);
    gStudentTable.draw();
  };

});