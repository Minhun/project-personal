-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th1 15, 2022 lúc 01:55 PM
-- Phiên bản máy phục vụ: 10.4.18-MariaDB
-- Phiên bản PHP: 8.0.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `school_db`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `class_room`
--

CREATE TABLE `class_room` (
  `id` bigint(20) NOT NULL,
  `class_code` varchar(255) DEFAULT NULL,
  `class_name` varchar(255) DEFAULT NULL,
  `teacher` varchar(255) DEFAULT NULL,
  `phone_number` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `class_room`
--

INSERT INTO `class_room` (`id`, `class_code`, `class_name`, `teacher`, `phone_number`) VALUES
(1, '12A1', 'Lớp chuyên ban A', 'Mr. Đào Trọng Kha', '0945362514'),
(2, '12C2', 'Lớp chuyên khối C', 'Ms. Ngô Minh Lý', '0987456123'),
(3, '12D5', 'Lớp chuyên khối D', 'Mr. Lê Minh Hải', '0963258741'),
(4, '12B13', 'Lớp chuyên khối B', 'Ms. Nguyễn Thị Kim Chi', '0985127463'),
(5, '12CB1', 'Lớp cơ bản ', 'Mr. Ngô Minh Hiếu', '03637845691');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `class_room`
--
ALTER TABLE `class_room`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_hrpfnikclhomq3wt07832cl0d` (`class_code`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `class_room`
--
ALTER TABLE `class_room`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
