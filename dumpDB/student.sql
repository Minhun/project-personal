-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th1 15, 2022 lúc 02:03 PM
-- Phiên bản máy phục vụ: 10.4.18-MariaDB
-- Phiên bản PHP: 8.0.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `school_db`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `student`
--

CREATE TABLE `student` (
  `id` bigint(20) NOT NULL,
  `student_code` varchar(255) DEFAULT NULL,
  `student_name` varchar(255) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `phone` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `class_room_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `student`
--

INSERT INTO `student` (`id`, `student_code`, `student_name`, `birthday`, `gender`, `phone`, `address`, `class_room_id`) VALUES
(1, '12A1023', 'Ninh Dương Lan Ngọc', '2004-09-16', 'Nữ', '0987456123', '8 Bạch Đằng, Q. Bình Thạnh', 1),
(2, '12C2034', 'Trương Thế Vinh', '2004-02-25', 'Nam', '0987456123', '100 Nguyễn Văn Trỗi, Phú Nhuận', 2),
(3, '12D5031', 'Võ Vũ Trường Giang', '2004-04-20', 'Nam', '0987445613', '20 Trần Quốc Thảo, Q.3', 3),
(4, '12B13017', 'Nguyễn Thùy Khánh Vân', '2004-07-19', 'Nữ', '0785963214', '100/c1 Xuân Thủy, Thảo Điền', 4),
(5, '12CB1009', 'Nguyễn Kiều Cẩm Thơ', '2004-08-10', 'Nữ', '0963741822', 'CC Cityland Gò Vấp', 5);

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_pilb3uo1cimnf1sp86nqcrjsv` (`student_code`),
  ADD KEY `FK3b5tt0fijrs22iefupqtw4397` (`class_room_id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `student`
--
ALTER TABLE `student`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `student`
--
ALTER TABLE `student`
  ADD CONSTRAINT `FK3b5tt0fijrs22iefupqtw4397` FOREIGN KEY (`class_room_id`) REFERENCES `class_room` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
