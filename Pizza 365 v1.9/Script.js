$(document).ready(function(){
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    // Lưu 1 biến toàn cục chứa dữ liệu loại combo được chọn
    // mỗi khi khách chọn menu S, M, L bạn lại đổi giá trị properties của nó
    var gSelectedMenuStructure = {
        menuName: "",    // S, M, L
        duongKinhCM: "",
        suongNuong: 0,
        saladGr: "",
        drink: 0,
        price: 0
    }
    // Lưu 1 biến toàn cục chứa dữ liệu loại pizza được chọn
    var gSelectedPizzaType = {
        typeName: "", //Hawai, Hải sản, thịt xông khói
    }
    // Danh sách đồ uống
    var gDrinkList = {};
    // Khai báo biến chứa dữ liệu đơn hàng
    var gOrderObj = {
        name: "",
        email: "",
        phone: 0,
        address: "",
        voucher: 0,
        message: "",
    };
    //Lưu 1 biến toàn cục chứa mã giảm giá sau khi check
    var gVoucher = {
        phanTramGiamGia: 0,
        voucherId: "",
    };
    /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
    onPageLoading();
    $("#btn-combo-s").on("click", function(){
        onBtnComboSClick();
    });
    $("#btn-combo-m").on("click", function(){
        onBtnComboMClick();
    });
    $("#btn-combo-l").on("click", function(){
        onBtnComboLClick();
    });
    $("#btn-sea-food").on('click', onBtnSeaFoodClick);
    $("#btn-hawaii").on('click', onBtnHawaiiClick);
    $("#btn-bacon").on('click', onBtnBaconClick);
    //Gán sự kiện cho nút gửi đơn hàng
    $("#btn-send-order").on("click", onBtnSendOrderClick);
    //Gán sự kiện cho nút quay lại trong modal
    $("#modal-btn-back").on("click", onBtnBackClick);
    //Gán sự kiện cho nút tạo đơn hàng trong modal
    $("#modal-btn-create").on("click", onBtnCreateOrderClick);
    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    //Hàm load trang
    function onPageLoading(){
        loadDrinkListToSelect();
    }
    //Hàm đổi màu khi chọn combo S
    function onBtnComboSClick(){
        //Thu thập dữ liệu menu S
        gSelectedMenuStructure.menuName = "Small";
        gSelectedMenuStructure.duongKinhCM = "20cm";
        gSelectedMenuStructure.suongNuong = 02;
        gSelectedMenuStructure.saladGr = "200g";
        gSelectedMenuStructure.drink = 02;
        gSelectedMenuStructure.price = 150.000;

        console.log(gSelectedMenuStructure);
        setMenuBtnColor(gSelectedMenuStructure.menuName)
    }
    //Hàm đổi màu khi chọn combo M
    function onBtnComboMClick(){
        //Thu thập dữ liệu menu M
        gSelectedMenuStructure.menuName = "Medium";
        gSelectedMenuStructure.duongKinhCM = "25cm";
        gSelectedMenuStructure.suongNuong = 04;
        gSelectedMenuStructure.saladGr = "300g";
        gSelectedMenuStructure.drink = 03;
        gSelectedMenuStructure.price = 200.000;

        console.log(gSelectedMenuStructure);
        setMenuBtnColor(gSelectedMenuStructure.menuName)
    }
    //Hàm đổi màu khi chọn combo L
    function onBtnComboLClick(){
        //Thu thập dữ liệu menu L
        gSelectedMenuStructure.menuName = "Large";
        gSelectedMenuStructure.duongKinhCM = "30cm";
        gSelectedMenuStructure.suongNuong = 08;
        gSelectedMenuStructure.saladGr = "500g";
        gSelectedMenuStructure.drink = 04;
        gSelectedMenuStructure.price = 250.000;

        console.log(gSelectedMenuStructure);
        setMenuBtnColor(gSelectedMenuStructure.menuName)
    }
    //Hàm đổi màu nút khi chọn pizza sea food
    function onBtnSeaFoodClick(){
        gSelectedPizzaType.typeName = "Sea Food";

        console.log(gSelectedPizzaType);
        setPizzaTypeBtnColor(gSelectedPizzaType.typeName);
    }
    //Hàm đổi màu nút khi chọn pizza hawaii
    function onBtnHawaiiClick(){
        gSelectedPizzaType.typeName = "Hawaii";

        console.log(gSelectedPizzaType);
        setPizzaTypeBtnColor(gSelectedPizzaType.typeName);
    }
    //Hàm đổi màu nút khi chọn pizza sea food
    function onBtnBaconClick(){
        gSelectedPizzaType.typeName = "Bacon";

        console.log(gSelectedPizzaType);
        setPizzaTypeBtnColor(gSelectedPizzaType.typeName);
    }
    //Hàm đổ drink list vào ô select
    function loadDrinkListToSelect(){
        //B1: Thu thập dữ liệu (ko có)
        //B2: Kiểm tra dữ liệu (ko có)
        //B3: Gọi Api
        $.ajax({
            url: "http://42.115.221.44:8080/devcamp-pizza365/drinks",
            type: "GET",
            dataType: 'json',
            success: function(drinkList){
                gDrinkList = drinkList;
                //B4: load dữ liệu order lên bảng
                loadDataDrinkToSelect(gDrinkList);
            },
            error: function(error){
            console.assert(error.responseText);
            }
        });
    }
    //Hàm xử lý sự kiện gửi đơn hàng
    function onBtnSendOrderClick(){
        //B1: Get data
        getDataOrder(gOrderObj);
        //B2: validate Data
        var vIsValid = validDateOrder(gOrderObj);
        if(vIsValid){
            //B3: call Api get discount voucher
            getPercentDiscount(gOrderObj);
            //B3: show order to modal
            showOrderToModal(gOrderObj);
        }
    }
    //Hàm xử lý việc click quay lại trong modal
    function onBtnBackClick(){
        $("#order-detail").modal("hide");
    }
    //
    function onBtnCreateOrderClick(){
        //ẩn modal tạo đơn
        $("#order-detail").modal("hide");
        //Khai báo đối tượng chứa dữ liệu
        var vOrderObjRequest = {
            kichCo: "",
            duongKinh: "",
            suon: "",
            salad: "",
            loaiPizza: "",
            idVourcher: "",
            idLoaiNuocUong: "",
            soLuongNuoc: "",
            hoTen: "",
            thanhTien: "",
            email: "",
            soDienThoai: "",
            diaChi: "",
            loiNhan: ""
        }
        //B1: Thu thập dữ liệu
        getOrderBy(vOrderObjRequest);
        //B2: Kiểm tra dữ liệu (ko cần)
        //B3: Gọi server để tạo đơn hàng
        callApiToCreateOrder(vOrderObjRequest);

    }
    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình */
    //Hàm xử lý việc set màu cho nút được chọn
    function setMenuBtnColor(paramSelectMenu){
        switch(paramSelectMenu){
            case "Small":
                $("#btn-combo-s").addClass("btn btn-success"); //green color
                break;
            case "Medium": 
                $("#btn-combo-m").addClass("btn btn-success");  //green color
                break;
            case "Large":
                $("#btn-combo-l").addClass("btn btn-success");  //green color
                break;
        }
    }
    //Hàm xử lý việc set màu cho nút kiểu pizza được chọn
    function setPizzaTypeBtnColor(paramSelectPizzaType){
        switch(paramSelectPizzaType){
            case "Sea Food":
                $("#btn-sea-food").addClass("btn btn-success"); //green color
                break;
            case "Hawaii": 
                $("#btn-hawaii").addClass("btn btn-success");  //green color
                break;
            case "Bacon":
                $("#btn-bacon").addClass("btn btn-success");  //green color
                break;
        }
    }
    //Hàm đổ dữ liệu đồ uống vào ô select
    function loadDataDrinkToSelect(paramDrinkList){
        //console.log(paramDrinkList);
        for(var bI = 0; bI < paramDrinkList.length; bI ++){
            $("#select-drink").append($('<option>',{
                value: paramDrinkList[bI].maNuocUong,
                text: paramDrinkList[bI].tenNuocUong
            }));
        }
    }
    //Hàm thu thập dữ liệu order
    function getDataOrder(paramOrderObj){
        paramOrderObj.name = $("#inp-name").val();
        paramOrderObj.email = $("#inp-email").val().trim();
        paramOrderObj.phone = $("#inp-phone").val();
        paramOrderObj.address = $("#inp-address").val().trim();
        paramOrderObj.voucher = $("#inp-voucher").val();
        paramOrderObj.message = $("#textarea-message").val();
    }
    //Hàm kiểm tra dữ liệu
    function validDateOrder(paramOrderObj){
        var vResult = true;
        //Format email
        var vRe = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(paramOrderObj.name === ""){
            alert("Vui lòng nhập tên");
            vResult = false;
        }
        if(paramOrderObj.email === "" || !vRe.test(paramOrderObj.email)){
            alert("Vui lòng nhập email");
            vResult = false;
        }
        if(isNaN(paramOrderObj.phone) || paramOrderObj.phone === ""){
            alert("Vui lòng nhập số điện thoại");
            vResult = false;
        }
        if(isNaN(paramOrderObj.voucher) || paramOrderObj.voucher === ""){
            alert("Vui lòng nhập số voucher");
            vResult = false;
        }
        if(paramOrderObj.address === ""){
            alert("Vui lòng nhập địa chỉ giao hàng");
            vResult = false;
        }
        if(gSelectedMenuStructure.menuName === ""){
            alert("Vui lòng chọn loại combo");
            vResult = false;
        }
        if(gSelectedPizzaType.typeName === ""){
            alert("Vui lòng chọn loại Pizza");
            vResult = false;
        }
        return vResult; 
    }
    //Hàm xử lý việc lấy được giá trị đối tượng voucher
    function getPercentDiscount(paramOrderObj){
        //B1: Thu thập dữ liệu (ko có)
        //B2: Kiểm tra dữ liệu (ko có)
        //B3: Gọi Api
        const vBASE_URL = "http://42.115.221.44:8080/devcamp-voucher-api/voucher_detail/";
        $.ajax({
            url: vBASE_URL + paramOrderObj.voucher,
            type: "GET",
            dataType: 'json',
            async: false,
            success: function(VoucherObj){
                console.log(VoucherObj);
                //console.log(VoucherObj.phanTramGiamGia);
                gVoucher.phanTramGiamGia = VoucherObj.phanTramGiamGia;
                gVoucher.voucherId = VoucherObj.id;
            }
        });
    }
    //Hàm hiển thị thông tin đơn hàng lên modal
    function showOrderToModal(paramOrderObj){
        //debugger;
        //console.log(paramOrderObj);
        
        var vDrink = $("#select-drink option:selected").text();
        //Gán dữ liệu cho ô text trong modal
        $("#modal-full-name").val(paramOrderObj.name);
        $("#modal-email").val(paramOrderObj.email);
        $("#modal-phone").val(paramOrderObj.phone);
        $("#modal-address").val(paramOrderObj.address);
        $("#modal-message").val(paramOrderObj.message);
        $("#modal-voucher").val(paramOrderObj.voucher);
        var vOrder = 
        "Xác nhận: " +
        paramOrderObj.name +
        ", " +
        paramOrderObj.phone +
        ", " +
        paramOrderObj.address +
        "&#13;&#10;" +
        "Menu: " + gSelectedMenuStructure.menuName +
        ", " +
        "sườn nướng: " + gSelectedMenuStructure.suongNuong + 
        ", " +
        "Salad: " + gSelectedMenuStructure.saladGr + 
        ", " +
        "nước uống: " + vDrink + ": " + gSelectedMenuStructure.drink + 
        "&#13;&#10;" +
        "Loại pizza: " + gSelectedPizzaType.typeName + 
        "&#13;&#10;" +
        "Giá: " + gSelectedMenuStructure.price + ".000 VNĐ" + 
        "&#13;&#10;" +
        "Giảm giá: " + gVoucher.phanTramGiamGia + "%" +
        "&#13;&#10;" + 
        "Phải thanh toán: " + (gSelectedMenuStructure.price * (100 - gVoucher.phanTramGiamGia)/100) + ".000 VNĐ"

        $("#modal-detail-order").html(vOrder);
        //Hiển thị modal
        $("#order-detail").modal("show");
    }
    //Hàm thu thập dữ liệu để tạo đơn hàng
    function getOrderBy(paramOrderObjRequest){
        paramOrderObjRequest.kichCo = gSelectedMenuStructure.menuName;
        paramOrderObjRequest.duongKinh = gSelectedMenuStructure.duongKinhCM;
        paramOrderObjRequest.suon = gSelectedMenuStructure.suongNuong;
        paramOrderObjRequest.salad = gSelectedMenuStructure.saladGr;
        paramOrderObjRequest.loaiPizza = gSelectedPizzaType.typeName;
        paramOrderObjRequest.idVourcher = gVoucher.voucherId;
        paramOrderObjRequest.idLoaiNuocUong = $("#select-drink option:selected").val();
        paramOrderObjRequest.soLuongNuoc = gSelectedMenuStructure.drink;
        paramOrderObjRequest.thanhTien = gSelectedMenuStructure.price;
        paramOrderObjRequest.hoTen = $("#inp-name").val();
        paramOrderObjRequest.email = $("#inp-email").val();
        paramOrderObjRequest.soDienThoai = $("#inp-phone").val();
        paramOrderObjRequest.diaChi = $("#inp-address").val();
        paramOrderObjRequest.loiNhan = $("#textarea-message").val()
    }
    //Hàm gọi server để tạo đơn hàng
    function callApiToCreateOrder(paramOrderObjRequest){
        $.ajax({
            url: "http://42.115.221.44:8080/devcamp-pizza365/orders",
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify(paramOrderObjRequest),
            success: function(paramOrderObjRequest){
                console.log(paramOrderObjRequest);
                //B4: Hiển thị order id lên modal
                showConfirmOrderToModal(paramOrderObjRequest);
                
            },
            error: function(error){
            console.assert(error.responseText);
            }
        });
    }
    //
    function showConfirmOrderToModal(paramOrderObjRequest){
        //Hiện modal mã đơn hàng
        $("#create-order").modal("show");
        $("#modal-order-id").val(paramOrderObjRequest.orderId);
    }

});