$(document).ready(function(){
    "use strict";
    //Chuyển trang product detail khi click nút xem thông tin
	$("#btn-product-detail").on("click", function(){
		onBtnProductCLick();
	});
	function onBtnProductCLick(){
		window.location.href = "Product.html";
	}
    var img = "img/Xe tay ga/AB/AB-Tiêu-chuẩn-Đỏ-bạc-đen-150.png";
    $("#select-color").on("change", function(){
        switch ($("#select-color").val()) {
            case "1":
                $("#img-product-detail").prop("src", "img/Xe tay ga/AB/AB-Đặc-biệt-Đen-125.png")
                $("#thumbimg-product-detail").prop("src", "img/Xe tay ga/AB/AB-Đặc-biệt-Đen-125.png")
                break;
            case "2":
                $("#img-product-detail").prop("src", "img/Xe tay ga/AB/AB-Tiêu-chuẩn-Đỏ-đen-125.png")
                $("#thumbimg-product-detail").prop("src", "img/Xe tay ga/AB/AB-Tiêu-chuẩn-Đỏ-đen-125.png")
                break;
            case "3":
                $("#img-product-detail").prop("src", "img/Xe tay ga/AB/AB-tiêu-chuẩn-Xanh-đen-125.png")
                $("#thumbimg-product-detail").prop("src", "img/Xe tay ga/AB/AB-tiêu-chuẩn-Xanh-đen-125.png")
                break;    
            default:
                $("#img-product-detail").prop("src", `${img}`)
                $("#thumbimg-product-detail").prop("src", `${img}`)
                break;
        }
    })
    
});