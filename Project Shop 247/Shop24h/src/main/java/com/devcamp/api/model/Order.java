package com.devcamp.api.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "orders")
public class Order {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "order_date")
	@CreatedDate
	@JsonFormat(pattern = "yyyy-mm-dd")
	private Date orderDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "required_date")
	@CreatedDate
	@JsonFormat(pattern = "yyyy-mm-dd")
	private Date requiredDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "shipped_date")
	@CreatedDate
	@JsonFormat(pattern = "yyyy-mm-dd")
	private Date shippedDate;
	
	@Column(name = "status", columnDefinition = "varchar(50)")
	private String status;
	
	@Column(name = "comment")
	private String commment;
	
	@ManyToOne(fetch = FetchType.LAZY)
	private Customer customer;
	
	@OneToMany(mappedBy = "order", cascade = CascadeType.ALL,
			fetch = FetchType.LAZY)
	private List<OrderDetail> orderDetail;
	
	//Contructor
	public Order() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Order(int id, Date orderDate, Date requiredDate, Date shippedDate, String status, String commment,
			Customer customer, List<OrderDetail> orderDetail) {
		super();
		this.id = id;
		this.orderDate = orderDate;
		this.requiredDate = requiredDate;
		this.shippedDate = shippedDate;
		this.status = status;
		this.commment = commment;
		this.customer = customer;
		this.orderDetail = orderDetail;
	}

	//getter & setter
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Date getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}
	public Date getRequiredDate() {
		return requiredDate;
	}
	public void setRequiredDate(Date requiredDate) {
		this.requiredDate = requiredDate;
	}
	public Date getShippedDate() {
		return shippedDate;
	}
	public void setShippedDate(Date shippedDate) {
		this.shippedDate = shippedDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCommment() {
		return commment;
	}
	public void setCommment(String commment) {
		this.commment = commment;
	}
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public List<OrderDetail> getOrderDetail() {
		return orderDetail;
	}

	public void setOrderDetail(List<OrderDetail> orderDetail) {
		this.orderDetail = orderDetail;
	}
	
}
