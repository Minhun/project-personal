package com.devcamp.api.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "payments")
public class Payment {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "check_number", columnDefinition = "varchar(50)")
	private String checkNumber;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "payment_date")
	@CreatedDate
	@JsonFormat(pattern = "yyyy-mm-dd")
	private Date paymentDate;
	
	@Column(name = "ammount")
	private double ammount;
	
	@ManyToOne(fetch = FetchType.LAZY)
	private Customer customer;
	
	//Contructor
	public Payment() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Payment(int id, String checkNumber, Date paymentDate, double ammount, Customer customer) {
		super();
		this.id = id;
		this.checkNumber = checkNumber;
		this.paymentDate = paymentDate;
		this.ammount = ammount;
		this.customer = customer;
	}
	//getter & setter
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCheckNumber() {
		return checkNumber;
	}
	public void setCheckNumber(String checkNumber) {
		this.checkNumber = checkNumber;
	}
	public Date getPaymentDate() {
		return paymentDate;
	}
	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}
	public double getAmmount() {
		return ammount;
	}
	public void setAmmount(double ammount) {
		this.ammount = ammount;
	}
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	
}
