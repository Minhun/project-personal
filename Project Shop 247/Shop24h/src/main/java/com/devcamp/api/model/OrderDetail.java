package com.devcamp.api.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "order_detail")
public class OrderDetail {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "quanity_order")
	private int quanityOrder;
	
	@Column(name = "price_each", columnDefinition = "DECIMAL(10,2)")
	private double priceEach;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	private Product product;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	private Order order;
	
	//Contructor
	public OrderDetail() {
		super();
		// TODO Auto-generated constructor stub
	}
	public OrderDetail(int id, int quanityOrder, double priceEach, Product product, Order order) {
		super();
		this.id = id;
		this.quanityOrder = quanityOrder;
		this.priceEach = priceEach;
		this.product = product;
		this.order = order;
	}
	//getter & setter
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getQuanityOrder() {
		return quanityOrder;
	}
	public void setQuanityOrder(int quanityOrder) {
		this.quanityOrder = quanityOrder;
	}
	public double getPriceEach() {
		return priceEach;
	}
	public void setPriceEach(double priceEach) {
		this.priceEach = priceEach;
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public Order getOrder() {
		return order;
	}
	public void setOrder(Order order) {
		this.order = order;
	}
	
}
