package com.devcamp.api.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "products")
public class Product {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "product_code", columnDefinition = "VARCHAR(50)")
	private String productCode;
	
	@Column(name = "product_name", columnDefinition = "VARCHAR(50)")
	private String prodcutName;
	
	@Column(name = "product_description")
	private String productDescription;
	
	@Column(name = "product_scale",columnDefinition = "VARCHAR(50)")
	private String productScale;
	
	@Column(name = "product_vendor", columnDefinition = "VARCHAR(50)")
	private String productVendor;
	
	@Column(name = "quantity_in_stock")
	private int quantityInStock;
	
	@Column(name = "buy_price", columnDefinition = "DECIMAL(10,2)")
	private double buyPrice;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	private ProductLine productLine;

	@OneToMany(mappedBy = "product", cascade = CascadeType.ALL,
			fetch = FetchType.LAZY)
	private List<OrderDetail> orderDetail;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	private Specifications specifications;
	//Contructor
	public Product() {
		super();
		// TODO Auto-generated constructor stub
	}

	//Getter & setter
	
	public Product(int id, String productCode, String prodcutName, String productDescription, String productScale,
			String productVendor, int quantityInStock, double buyPrice, ProductLine productLine,
			List<OrderDetail> orderDetail, Specifications specifications) {
		super();
		this.id = id;
		this.productCode = productCode;
		this.prodcutName = prodcutName;
		this.productDescription = productDescription;
		this.productScale = productScale;
		this.productVendor = productVendor;
		this.quantityInStock = quantityInStock;
		this.buyPrice = buyPrice;
		this.productLine = productLine;
		this.orderDetail = orderDetail;
		this.specifications = specifications;
	}

	public int getId() {
		return id;
	}

	
	public Specifications getSpecifications() {
		return specifications;
	}

	public void setSpecifications(Specifications specifications) {
		this.specifications = specifications;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return prodcutName;
	}

	public void setProductName(String productName) {
		this.prodcutName = productName;
	}

	public String getProductDescription() {
		return productDescription;
	}

	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}

	public String getProductScale() {
		return productScale;
	}

	public void setProductScale(String productScale) {
		this.productScale = productScale;
	}

	public String getProdcutName() {
		return prodcutName;
	}

	public void setProdcutName(String prodcutName) {
		this.prodcutName = prodcutName;
	}

	public String getProductVendor() {
		return productVendor;
	}

	public void setProductVendor(String productVendor) {
		this.productVendor = productVendor;
	}

	public int getQuantityInStock() {
		return quantityInStock;
	}

	public void setQuantityInStock(int quantityInStock) {
		this.quantityInStock = quantityInStock;
	}

	public double getBuyPrice() {
		return buyPrice;
	}

	public void setBuyPrice(double buyPrice) {
		this.buyPrice = buyPrice;
	}

	public ProductLine getProductLine() {
		return productLine;
	}

	public void setProductLine(ProductLine productLine) {
		this.productLine = productLine;
	}


	public List<OrderDetail> getOrderDetail() {
		return orderDetail;
	}


	public void setOrderDetail(List<OrderDetail> orderDetail) {
		this.orderDetail = orderDetail;
	}
	
}
