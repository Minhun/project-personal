package com.devcamp.api.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "customers")
public class Customer {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "last_name", columnDefinition = "varchar(50)")
	private String lastName;
	
	@Column(name = "first_name", columnDefinition = "varchar(50)")
	private String firstName;
	
	@Column(name = "phone_number")
	private String phoneNumber;
	
	@Column(name = "address")
	private String address;
	
	@Column(name = "city", columnDefinition = "varchar(50)")
	private String city;
	
	@Column(name = "state", columnDefinition = "varchar(50)")
	private String state;
	
	@Column(name = "postal_code", columnDefinition = "varchar(50)")
	private String postalCode;
	
	@Column(name = "country", columnDefinition = "varchar(50)")
	private String country;
	
	@OneToMany(mappedBy = "customer", cascade = CascadeType.ALL,
			fetch = FetchType.LAZY)
	private List<Payment> payments;
	
	@OneToMany(mappedBy = "customer",cascade = CascadeType.ALL,
			fetch = FetchType.LAZY)
	private List<Order> orders;
	
	//Contructor
	public Customer() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	public Customer(int id, String lastName, String firstName, String phoneNumber, String address, String city,
			String state, String postalCode, String country, List<Payment> payments, List<Order> orders) {
		super();
		this.id = id;
		this.lastName = lastName;
		this.firstName = firstName;
		this.phoneNumber = phoneNumber;
		this.address = address;
		this.city = city;
		this.state = state;
		this.postalCode = postalCode;
		this.country = country;
		this.payments = payments;
		this.orders = orders;
	}

	//getter & setter
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public List<Payment> getPayments() {
		return payments;
	}
	public void setPayments(List<Payment> payments) {
		this.payments = payments;
	}
	public List<Order> getOrders() {
		return orders;
	}
	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}
}
