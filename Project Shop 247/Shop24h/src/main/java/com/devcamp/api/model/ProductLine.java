package com.devcamp.api.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "product_lines")
public class ProductLine {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "product_line")
	private String productLine;
	
	@Column(name = "description")
	private String description;
	
	@OneToMany(mappedBy = "productLine", cascade = CascadeType.ALL,
			fetch = FetchType.LAZY)
	private List<Product> product;
	
	//Contructor
	public ProductLine() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public ProductLine(int id, String productLine, String description, List<Product> product) {
		super();
		this.id = id;
		this.productLine = productLine;
		this.description = description;
		this.product = product;
	}

	//Getter & setter
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getProductLine() {
		return productLine;
	}
	public void setProductLine(String productLine) {
		this.productLine = productLine;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public List<Product> getProduct() {
		return product;
	}

	public void setProduct(List<Product> product) {
		this.product = product;
	}
	
}
