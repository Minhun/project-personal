package com.devcamp.api.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "thong_so_KT")
public class Specifications {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "khoi_luong", columnDefinition = "VARCHAR(50)")
	private String khoiLuong;
	
	@Column(name = "kich_thuoc", columnDefinition = "VARCHAR(50)")
	private String kichThuoc;
	
	@Column(name = "dung_tich_binh_xang", columnDefinition = "VARCHAR(50)")
	private String dungTichXang;
	
	@Column(name = "dong_co", columnDefinition = "VARCHAR(50)")
	private String dongCo;
	
	@Column(name = "piston", columnDefinition = "VARCHAR(50)")
	private String piston;
	
	@Column(name = "cong_suat", columnDefinition = "VARCHAR(50)")
	private String congSuat;
	
	@OneToMany(mappedBy = "specifications", cascade = CascadeType.ALL,
			fetch = FetchType.LAZY)
	private List<Product> product;
	
	//contructor
	public Specifications() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Specifications(int id, String khoiLuong, String kichThuoc, String dungTichXang, String dongCo, String piston,
			String congSuat, List<Product> product) {
		super();
		this.id = id;
		this.khoiLuong = khoiLuong;
		this.kichThuoc = kichThuoc;
		this.dungTichXang = dungTichXang;
		this.dongCo = dongCo;
		this.piston = piston;
		this.congSuat = congSuat;
		this.product = product;
	}

	//getter & setter
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getKhoiLuong() {
		return khoiLuong;
	}
	public void setKhoiLuong(String khoiLuong) {
		this.khoiLuong = khoiLuong;
	}
	public String getKichThuoc() {
		return kichThuoc;
	}
	public void setKichThuoc(String kichThuoc) {
		this.kichThuoc = kichThuoc;
	}
	public String getDungTichXang() {
		return dungTichXang;
	}
	public void setDungTichXang(String dungTichXang) {
		this.dungTichXang = dungTichXang;
	}
	public String getDongCo() {
		return dongCo;
	}
	public void setDongCo(String dongCo) {
		this.dongCo = dongCo;
	}
	public String getPiston() {
		return piston;
	}
	public void setPiston(String piston) {
		this.piston = piston;
	}
	public String getCongSuat() {
		return congSuat;
	}
	public void setCongSuat(String congSuat) {
		this.congSuat = congSuat;
	}

	public List<Product> getProduct() {
		return product;
	}

	public void setProduct(List<Product> product) {
		this.product = product;
	}
	
}
