"use strict";
$(document).ready(function(){
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    //Hằng số các column
    const gORDER_ID_COL = 0;
    const gKICH_CO_COL = 1;
    const gPIZZA_TYPE_COL = 2;
    const gDRINK_COL = 3;
    const gPRICE_COL = 4 ;
    const gFULL_NAME_COL = 5;
    const gPHONE_COL = 6;
    const gSTATUS_COL = 7;
    const gCOLUMN_ACTION = 8;

    //Hằng số chứa các thuộc tính của bảng
    const gORDER = ["orderId", "kichCo", "loaiPizza", "idLoaiNuocUong", "thanhTien", "hoTen", "soDienThoai", "trangThai", "action"]
    //Biến toàn cục chứa mảng dữ liệu order khi api trả về
    var gOrderDB = {
        order: [],
        //các phương thức làm việc với mảng order
        filterOrder: function(paramOrderFilterObj){
            var vOrderFilterResult = [];
            vOrderFilterResult = this.order.filter(function(paramOrder){
                return (
                    (paramOrder.trangThai == paramOrderFilterObj.trangthai 
                    || paramOrderFilterObj.trangthai == "all") &&
                    (paramOrder.loaiPizza == paramOrderFilterObj.loaipizza 
                    || paramOrderFilterObj.loaipizza == "all")
                );
            });
            return vOrderFilterResult;
        }
    };
    //Biến toàn cục id & orderID
    var gId = 0;
    var gOrderId = "";
    //Lưu 1 biến toàn cục chứa dữ liệu row khi click nút chi tiết
    var gRowDataOrder = [];
    //định nghĩa table
    var gOrderTable = $("#table-order").DataTable({
        columns: [
            {"data": gORDER[gORDER_ID_COL]},
            {"data": gORDER[gKICH_CO_COL]},
            {"data": gORDER[gPIZZA_TYPE_COL]},
            {"data": gORDER[gDRINK_COL]},
            {"data": gORDER[gPRICE_COL]},
            {"data": gORDER[gFULL_NAME_COL]},
            {"data": gORDER[gPHONE_COL]},
            {"data": gORDER[gSTATUS_COL]},
            {"data": gORDER[gCOLUMN_ACTION]}
        ],
        "columnDefs": [ 
            {//định nghĩa cột action
                "targets": gCOLUMN_ACTION,
                "defaultContent": `
                <button class="btn btn-info detail"><i class="fas fa-info-circle" data-toggle="tooltip" 
                data-placement="top" title="Sửa"></i> Chi tiết</button> 
                `
            }]
    });
    /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
    onPageLoading();
    //Gán sự kiện cho nút detail
    $("#table-order").on("click", ".detail", function(){
        onBtnDetailClick(this);
    });
    //Gán sự kiện cho nút filter
    $("#btn-filter-order").on("click", onBtnFilterClick);
    //Gán sự kiện cho nút confirm đơn hàng
    $("#btn-confirm-order").on("click", onBtnConfirmClick);
    //Gán sư kiện cho nút cancel đơn hàng
    $("#btn-cancel-order").on("click", onBtnCancelClick);
    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    //Hàm load trang
    function onPageLoading(){
        //call Api to get all order
        getAllOrderFromApi();

    }
    //Hàm xử lý sự kiện cho nút chi tiết
    function onBtnDetailClick(paramDetail){
        //console.log("Nút detail được bấm");
        //Lấy được dữ liệu của dòng thông qua nút chi tiết
        gRowDataOrder = getDataFromButton(paramDetail);

        gId = gRowDataOrder.id;
        gOrderId = gRowDataOrder.orderId;

        //Hiển thị thông tin lên console
        /*console.log(gRowDataOrder);
        console.log("id: " + gId);
        console.log("id: " + gOrderId);
        */
    
        //Gọi api để lấy order detail
        getOrderDetail(gOrderId);
        //Hiển thị bảng modal
        $("#order-detail").modal("show");
    }
    //Hàm xử lý việc filter
    function onBtnFilterClick(){
        //debugger;
        //Khai báo biến chứa đối tượng filter
        var vOrderFilterObj = {
            trangthai: "",
            loaipizza: ""
        }
        //B1: thu thập dữ liệu
        getFormData(vOrderFilterObj);
        //B2: Validate data (ko có)
        //B3: Xử lý nghiệp vụ lọc dữ liệu
        var vOrderFilter = gOrderDB.filterOrder(vOrderFilterObj);
        //B4: Hiển thị kết quả lên bảng
        console.log(vOrderFilterObj);
        loadDataToTable(vOrderFilter);
    }
    //Hàm xử lý việc confirm đơn hàng
    function onBtnConfirmClick(){
        //B1: get data
        var vObjectRequest = {
            trangThai: "confirmed"
        } 
        //B2: validate data
        if(gId !== null){
            //B3: call Api to update status
            onUpdateConfirmOrder(vObjectRequest, gId);
        }
    }
    //Hàm xử lý việc confirm đơn hàng
    function onBtnCancelClick(){
        //B1: get data
        var vObjectRequest = {
            trangThai: "cancel"
        } 
        //B2: validate data
        if(gId !== null){
            //B3: call Api to update status
            onUpdateConfirmOrder(vObjectRequest, gId);
        }
    }
    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình */
    //Hàm xử lý việc gọi Api để lấy data order
    function getAllOrderFromApi(){
        //B1: Thu thập dữ liệu (ko có)
        //B2: validate data (ko có)
        //B3: Gọi server
        $.ajax({
            url: "http://42.115.221.44:8080/devcamp-pizza365/orders",
            type: "GET",
            dataType: 'json',
            success: function(responseObject){
                //debugger;
                gOrderDB.order = responseObject;
                //console.log(gOrderDB.order);
                //B4: load dữ liệu order lên bảng
                loadDataToTable(gOrderDB.order);
            },
            error: function(error){
            console.assert(error.responseText);
            }
        });
    }
    //Hàm load dữ liệu order lên table
    function loadDataToTable(paramOrderArr){
        gOrderTable.clear()
        gOrderTable.rows.add(paramOrderArr);
        gOrderTable.draw();
    }
    //Hàm xác định row của bảng order khi click vào nút chi tiết
    function getDataFromButton(paramDetail){
        var vTableRow = $(paramDetail).parents("tr");
        var vRowDataOrder = gOrderTable.row(vTableRow).data();
        //console.log(vUserRowData);
        return vRowDataOrder ;

    }
    //Hàm thu thập dữ liệu
    function getFormData(paramOrderFilterObj){
        paramOrderFilterObj.trangthai = $("#select-status").val();
        paramOrderFilterObj.loaipizza = $("#select-pizza-type").val();
    }
    //Hàm gọi api lấy dữ liệu order detail
    function getOrderDetail(paramOrderId){
        const gBASE_URL = "http://42.115.221.44:8080/devcamp-pizza365/orders";
        //B1: Thu thập dữ liệu (ko có)
        //B2: validate data (ko có)
        //B3: Gọi server
        $.ajax({
            url: gBASE_URL + "/" + paramOrderId,
            type: "GET",
            dataType: 'json',
            success: function(responseObject){
                console.log(responseObject);
                gId = responseObject.id;
                //B4: load dữ liệu order lên bảng
                showOrderDetailToModal(responseObject);
            },
            error: function(error){
            console.assert(error.responseText);
            }
        });
    }
    //Hàm hiển thị order detail lên modal
    function showOrderDetailToModal(responseObject){
        $("#inp-order-id").val(responseObject.orderId);
        $("#inp-diameter").val(responseObject.duongKinh);
        $("#inp-meet-ribs").val(responseObject.suon);
        $("#inp-drink-quantity").val(responseObject.soLuongNuoc);
        $("#inp-voucher-id").val(responseObject.idVourcher);
        $("#inp-salad").val(responseObject.salad);
        $("#inp-payment").val(responseObject.thanhTien);
        $("#inp-discount").val(responseObject.giamGia);
        $("#inp-full-name").val(responseObject.hoTen);
        $("#inp-email").val(responseObject.email);
        $("#inp-phone-number").val(responseObject.soDienThoai);
        $("#inp-adress").val(responseObject.diaChi);
        $("#inp-status-order").val(responseObject.trangThai);
        $("#inp-date-create").val(responseObject.ngayTao);
        $("#inp-date-update").val(responseObject.ngayCapNhat);
        $("#textarea-message").val(responseObject.loiNhan);
        $("#select-size option:selected").text(responseObject.kichCo);
        $("#select-drink option:selected").text(responseObject.idLoaiNuocUong);
        $("#select-pizza option:selected").text(responseObject.loaiPizza);
    }
    //Hàm gọi server để update status order
    function onUpdateConfirmOrder(paramObjRequest, paramId){
        $.ajax({
            url: "http://42.115.221.44:8080/devcamp-pizza365/orders/" + paramId,
            type: "PUT",
            contentType: "application/json",
            data: JSON.stringify(paramObjRequest),
            success: function (detailOrder) {
                //B4: Check status on order list
                location.reload();
            },
            error: function () {
                alert("Repsonse null");
            }
        });
    }
});