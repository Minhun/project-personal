package com.devcamp.api.controller;

import java.awt.dnd.DropTargetEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.api.model.Classroom;
import com.devcamp.api.model.Student;
import com.devcamp.api.repository.IClassRoomResponsitory;
import com.devcamp.api.repository.IStudentResponsitory;

@CrossOrigin
@RestController
public class StudentController {
	@Autowired
	IStudentResponsitory pStudentResponsitory;
	
	@Autowired
	IClassRoomResponsitory pClassRoomResponsitory;
	
	//get all student
	@CrossOrigin
	@GetMapping("/student/all")
	public ResponseEntity<List<Student>> getAllStudent(){
		try {
			List<Student> students = new ArrayList<Student>();
			pStudentResponsitory.findAll().forEach(students::add);
			return new ResponseEntity<>(students, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	//get student by id
	@CrossOrigin
	@GetMapping("/student/detail/{id}")
	public ResponseEntity<Student> getStudentById(@PathVariable(value = "id")Long id){
		try {
			Optional<Student> studentData = pStudentResponsitory.findById(id);
			if (studentData.isPresent()) {
				return new ResponseEntity<>(studentData.get(), HttpStatus.OK);
			}else {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	//get student by classId
	@CrossOrigin
	@GetMapping("/student/{classId}/class")
	public ResponseEntity<List<Student>> getStudentByClassId(@PathVariable(value = "classId")Long classID){
		try {
			if (pClassRoomResponsitory.findById(classID).isPresent()) {
				List<Student> vStudent = new ArrayList<Student>();
				vStudent = pStudentResponsitory.findByClassRoomId(classID);
				return new ResponseEntity<>(vStudent, HttpStatus.OK);
			}else {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	//Create new student by classID
	@CrossOrigin
	@PostMapping("/student/create/{classId}")
	public ResponseEntity<Object> createStudent(@PathVariable(value = "classId")Long classId, @RequestBody Student vStudent){
		try {
			Optional<Classroom> classData = pClassRoomResponsitory.findById(classId);
			if (classData.isPresent()) {
				if (pStudentResponsitory.existsByStudentCode(vStudent.getStudentCode())) {
					Map<Student, String> objectApi = new HashMap<>();
					objectApi.put(vStudent, "Mã số sinh viên đã tồn tại");
					return new ResponseEntity<>(objectApi, HttpStatus.BAD_REQUEST);
				}else {
					Student newStudent = new Student();
					newStudent.setClassRoom(vStudent.getClassRoom());
					newStudent.setStudentCode(vStudent.getStudentCode());
					newStudent.setStudentName(vStudent.getStudentName());
					newStudent.setAddress(vStudent.getAddress());
					newStudent.setBirthday(vStudent.getBirthday());
					newStudent.setGender(vStudent.getGender());
					newStudent.setPhone(vStudent.getPhone());
					
					Classroom _class = classData.get();
					newStudent.setClassRoom(_class);
					
					Student _student = pStudentResponsitory.save(newStudent);
					return new ResponseEntity<>(_student, HttpStatus.OK);
				}
			}else {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
		}
	}
	//Update student by id
	@CrossOrigin
	@PutMapping("/student/update/{id}")
	public ResponseEntity<Object> updateStudent(@PathVariable(value = "id")Long id, @RequestBody Student vStudent){
		try {
			Optional<Student> studentData = pStudentResponsitory.findById(id);
			if (studentData.isPresent()) {
				Student newStudent = studentData.get();
				newStudent.setStudentCode(vStudent.getStudentCode());
				newStudent.setStudentName(vStudent.getStudentName());
				newStudent.setAddress(vStudent.getAddress());
				newStudent.setBirthday(vStudent.getBirthday());
				newStudent.setGender(vStudent.getGender());
				Student _student = pStudentResponsitory.save(newStudent);
				return new ResponseEntity<>(_student, HttpStatus.OK);
			}else {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
		}
	}
	//delete all student
	@CrossOrigin
	@DeleteMapping("/student/delete/all")
	public ResponseEntity<Student> deleteAllStudent(){
		try {
			pStudentResponsitory.deleteAll();
			return new ResponseEntity<>(HttpStatus.ACCEPTED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	//delete student by id
	@CrossOrigin
	@DeleteMapping("/student/delete/{id}")
	public ResponseEntity<Student> deleteStudentById(@PathVariable(value = "id")Long id){
		try {
			if (pStudentResponsitory.findById(id).isPresent()) {
				pStudentResponsitory.deleteById(id);
				return new ResponseEntity<>(HttpStatus.ACCEPTED);
			}else {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
