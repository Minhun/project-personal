package com.devcamp.api.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.api.model.Classroom;
import com.devcamp.api.model.Student;
import com.devcamp.api.repository.IClassRoomResponsitory;
import com.devcamp.api.repository.IStudentResponsitory;

@CrossOrigin
@RestController
public class ClassRoomController {
	@Autowired
	IClassRoomResponsitory pClassRoomResponsitory;
	
	@Autowired
	IStudentResponsitory pStudentResponsitory;
	
	//get all class
	@CrossOrigin
	@GetMapping("/class/all")
	public ResponseEntity<List<Classroom>> getAllClass(){
		try {
			List<Classroom> vClass = new ArrayList<Classroom>();
			pClassRoomResponsitory.findAll().forEach(vClass::add);
			return new ResponseEntity<>(vClass, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	//get class by id
	@CrossOrigin
	@GetMapping("/class/detail/{id}")
	public ResponseEntity<Classroom> getClassById(@PathVariable(value = "id")Long id){
		Optional<Classroom> classData = pClassRoomResponsitory.findById(id);
		try {
			if (classData.isPresent()) {
				return new ResponseEntity<>(classData.get(), HttpStatus.OK);
			}else {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	//get class by classCode
	@CrossOrigin
	@GetMapping("/class/{classCode}")
	public ResponseEntity<Classroom> getClassByClassCode(@PathVariable(value = "classCode")String classCode){
		Optional<Classroom> classData = pClassRoomResponsitory.findByClassCode(classCode);
		try {
			if (classData.isPresent()) {
				return new ResponseEntity<>(classData.get(), HttpStatus.OK);
			}else {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	@CrossOrigin
	@GetMapping("/class/{studentId}/student")
	public ResponseEntity<Object> getClassByStudentId(@PathVariable(value = "studentId")Long studentId){
		Optional<Student> studentData = pStudentResponsitory.findById(studentId);
		try {
			if (studentData.isPresent()) {
				Optional<Classroom> vClassRoom = pClassRoomResponsitory.findByStudentId(studentId);
				return new ResponseEntity<>(vClassRoom, HttpStatus.OK);
			}else {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	//create class room
	@CrossOrigin
	@PostMapping("/class/create")
	public ResponseEntity<Object> createClass(@RequestBody Classroom cClass){
		try {
			if (pClassRoomResponsitory.existsByClassCode(cClass.getClassCode())) {
				Map<Classroom, String> objectApi = new HashMap<>();
				objectApi.put(cClass, "Mã số lớp học đã tồn tại");
				return new ResponseEntity<>(objectApi, HttpStatus.BAD_REQUEST);
			}
			Classroom newClass = new Classroom();
			newClass.setClassCode(cClass.getClassCode());
			newClass.setClassName(cClass.getClassName());
			newClass.setTeacher(cClass.getTeacher());
			newClass.setPhoneNumber(cClass.getPhoneNumber());
			
			Classroom _class = pClassRoomResponsitory.save(newClass);
			return new ResponseEntity<>(_class, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
		}
	}
	//update class room by id
	@CrossOrigin
	@PutMapping("/class/update/{id}")
	public ResponseEntity<Object> updateClass(@PathVariable(value = "id")Long id, @RequestBody Classroom vClass){
		try {
			Optional<Classroom> classData = pClassRoomResponsitory.findById(id);
			if (classData.isPresent()) {
				Classroom newClass = classData.get();
				newClass.setClassCode(vClass.getClassCode());
				newClass.setClassName(vClass.getClassName());
				newClass.setTeacher(vClass.getTeacher());
				newClass.setPhoneNumber(vClass.getPhoneNumber());
				Classroom savedClass = pClassRoomResponsitory.save(newClass);
				return new ResponseEntity<>(savedClass, HttpStatus.OK);
			}else {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
		}
	}
	//delete all class room
	@CrossOrigin
	@DeleteMapping("/class/delete/all")
	public ResponseEntity<Classroom> deleteALlClass(){
		try {
			pClassRoomResponsitory.deleteAll();
			return new ResponseEntity<>(HttpStatus.ACCEPTED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	//delete class by id
	@CrossOrigin
	@DeleteMapping("/class/delete/{id}")
	public ResponseEntity<Classroom> deleteClassByID(@PathVariable(value = "id")Long id){
		try {
			Optional<Classroom> classData = pClassRoomResponsitory.findById(id);
			if (classData.isPresent()) {
				pClassRoomResponsitory.deleteById(id);
				return new ResponseEntity<>(HttpStatus.ACCEPTED);
			}else {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
}
