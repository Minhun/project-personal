package com.devcamp.api.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "class_room")
public class Classroom {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@NotEmpty(message = "Vui lòng nhập mã lớp")
	@Column(name = "class_code", unique = true)
	private String classCode;
	
	@NotBlank(message = "Vui lòng nhập tên lớp")
	@Column(name = "class_name")
	private String className;
	
	@NotBlank(message = "Vui lòng nhập tên GVCN")
	@Column(name = "teacher")
	private String teacher;
	
	@NotNull(message = "Nhập số điện thoại")
	@Column(name = "phone_number")
	private String phoneNumber;
	
	@OneToMany(targetEntity = Student.class, cascade = CascadeType.ALL)
	@JoinColumn(name = "class_room_id")
	private List<Student> student;
	
	//contructor
	public Classroom() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Classroom(long id, @NotEmpty(message = "Vui lòng nhập mã lớp") String classCode,
			@NotBlank(message = "Vui lòng nhập tên lớp") String className,
			@NotBlank(message = "Vui lòng nhập tên GVCN") String teacher,
			@NotNull(message = "Nhập số điện thoại") String phoneNumber, List<Student> student) {
		super();
		this.id = id;
		this.classCode = classCode;
		this.className = className;
		this.teacher = teacher;
		this.phoneNumber = phoneNumber;
		this.student = student;
	}

	//getter & setter
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getClassCode() {
		return classCode;
	}

	public void setClassCode(String classCode) {
		this.classCode = classCode;
	}

	public String getTeacher() {
		return teacher;
	}

	public void setTeacher(String teacher) {
		this.teacher = teacher;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public List<Student> getStudent() {
		return student;
	}

	public void setStudent(List<Student> student) {
		this.student = student;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.classCode;
	}
	
}
