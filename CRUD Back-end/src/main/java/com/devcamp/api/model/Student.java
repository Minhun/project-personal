package com.devcamp.api.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.CreatedDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "student")
public class Student {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@NotEmpty(message = "Vui lòng nhập mã học sinh")
	@Column(name = "student_code", unique = true)
	private String studentCode;
	
	@NotBlank(message = "Vui lòng nhập tên học sinh")
	@Column(name = "student_name")
	private String studentName;
	
	@Column(name = "gender")
	private String gender;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "birthday")
	@CreatedDate
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date birthday;
	
	@NotNull(message = "Nhập địa chỉ")
	@Column(name = "address")
	private String address;
	
	@NotNull(message = "Nhập số điện thoại")
	@Column(name = "phone")
	private String phone;
	
	@ManyToOne
	@JsonIgnore
	private Classroom classRoom;
	
	//contructor
	

	public Student() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Student(long id, @NotEmpty(message = "Vui lòng nhập mã học sinh") String studentCode,
			@NotBlank(message = "Vui lòng nhập tên học sinh") String studentName, String gender, Date birthday,
			@NotNull(message = "Nhập địa chỉ") String address, @NotNull(message = "Nhập số điện thoại") String phone,
			Classroom classRoom) {
		super();
		this.id = id;
		this.studentCode = studentCode;
		this.studentName = studentName;
		this.gender = gender;
		this.birthday = birthday;
		this.address = address;
		this.phone = phone;
		this.classRoom = classRoom;
	}

	//getter & setter
	
	public long getId() {
		return id;
	}

	public String getStudentCode() {
		return studentCode;
	}

	public void setStudentCode(String studentCode) {
		this.studentCode = studentCode;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Classroom getClassRoom() {
		return classRoom;
	}

	public void setClassRoom(Classroom classRoom) {
		this.classRoom = classRoom;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.studentCode;
	}
	
}
