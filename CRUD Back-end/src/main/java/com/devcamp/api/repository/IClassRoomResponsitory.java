package com.devcamp.api.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.api.model.Classroom;

public interface IClassRoomResponsitory extends JpaRepository<Classroom, Long> {
	Optional<Classroom> findByClassCode(String classCode);


	Optional<Classroom> findByStudentId(Long studentId);


	boolean existsByClassCode(String classCode);

}
