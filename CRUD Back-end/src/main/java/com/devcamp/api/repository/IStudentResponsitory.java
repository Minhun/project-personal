package com.devcamp.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.api.model.Student;

public interface IStudentResponsitory extends JpaRepository<Student, Long> {

	List<Student> findByClassRoomId(Long classRoomId);

	boolean existsByStudentCode(String studentCode);

}
